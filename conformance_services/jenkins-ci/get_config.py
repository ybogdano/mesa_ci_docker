#! /bin/python3

import os
from git import Repo
import shutil
import yaml
import copy
import tempfile

git_repo = "git://otc-mesa-ci.local/git/mirror/mesa_ci_config/origin"
repo_path = "/tmp/mesa_ci_config"
creds_file = "./credentials.yaml.tmpl"
combined_file = "./configuration/combined_config.yaml"

# update local config from repo
def update_config():
    with tempfile.TemporaryDirectory() as tmpd:
        print("Cloning mesa_ci_config repo...")
        Repo.clone_from(git_repo, tmpd)
        print("Combining credentials with CASC config...")

        creds_config = None
        combined_config = None
        with open(os.path.join(tmpd, "configuration/jenkins.yaml")) as fh:
            combined_config = yaml.safe_load(fh)
        with open(creds_file) as fh:
            creds_config = yaml.safe_load(fh)

        combined_config.pop('groovy', None)
        combined_config.pop('credentials', None)
        combined_config.update(copy.deepcopy(creds_config))
        with open(combined_file, 'w') as fh:
            yaml.safe_dump(combined_config, stream=fh, default_flow_style=False)

        print("Configs combined.")

if __name__ == "__main__":
    update_config()
