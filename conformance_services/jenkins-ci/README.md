Setting up authentication for Intel Mesa CI subjobs:

NB: SOME PATHS MAY NEED TO BE CHANGED IN docker-compose.yaml. CHANGE AS NEEDED.

1) Generate an api token for the account. This should be a 34-digit alpha-numeric string.
2) Copy the token to the filepath  ./secrets/jenkins_auth
   - make sure there is no whitespace around the token content, including newlines.
2a) Copy the token file to /var/cache/mesa_ci_docker/conformance_services/jenkins_ci/configuration/jenkins_auth on the ci server (otc-mesa-ci.jf.intel.com)
    - enables fetch_mirrors to trigger jobs
3) Copy the jenkins account's ssh private key to the file path ./secrets/ssh_private_key
4)  Create credentials containing the new tokens/passwords, by re-generating the jenkins instance:
    `./docker-compose.sh down`
    `./docker-compose.sh up -d`
    NB: be sure to run ./docker-compose.sh and NOT simply the docker-compose command
5) Check the jenkins CI instance to verify that the credentials from 2) and 3) are created (Username dropdown on top banner of jenkins webpage -> Credentials)
6) Check that CI can connect to test systems
7) Run a test jenkins job that uses the credentials to verify they're working properly
