#!/bin/bash
set -e

dne=false

if [ ! -r ./secrets/jenkins_auth ]; then
    echo "./secrets/jenkins_auth not found!"
    dne=true
fi
if [ ! -r ./secrets/ssh_private_key ]; then
    echo "./secrets/ssh_private_key not found!"
    dne=true
fi

if [ $dne = true ]; then
    echo "Container start aborted. Exiting..."
    exit 1
fi

python3 get_config.py
docker-compose "$@"
