#!/usr/bin/env python3

import os
import yaml
import subprocess


def get_token():
    """copy jenkins auth token to expected dir"""

    scp_path = None
    try:
        with open("./vars.yml") as fh:
            scp_path = yaml.safe_load(fh)["jenkins_auth_scp_path"]
        os.makedirs("./jenkins-ci/secrets", exist_ok=True)
        subprocess.check_call(["scp",
                               scp_path,
                               "./jenkins-ci/secrets/jenkins_auth"])
    except (FileNotFoundError, KeyError, subprocess.CalledProcessError):
        print("ERROR: jenkins_auth_scp_path not set in ./vars.yml")


if __name__ == "__main__":
    get_token()
