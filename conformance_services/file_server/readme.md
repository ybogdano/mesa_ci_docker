## File Server is read only from web with direct link download
to share files:
    - drop files in to directory listed in ```docker-compose.yml``` file
    - refresh browser, and let the server do it's work
