import os
from core import app
# from waitress import serve

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=os.environ.get("FLASK_SERVER_PORT", 8000))
    # serve(app, host='0.0.0.0', port=os.environ.get("FS_PORT", 8000))
