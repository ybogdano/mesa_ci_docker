from flask import Flask
from decouple import config

app = Flask(__name__)
app.config.from_object(config("APP_SETTINGS"))

from core import routes

path_view = routes.PathView.as_view('path_view')
app.add_url_rule('/', view_func=path_view)
app.add_url_rule('/<path:p>', view_func=path_view)

