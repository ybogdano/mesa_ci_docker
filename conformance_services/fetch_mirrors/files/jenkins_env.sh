#!/bin/bash

# read jenkins token and execute command with environment set

export JENKINS_PW=$(cat /var/run/secrets/jenkins_auth | xargs)
export JENKINS_USER=jenkins

$@
