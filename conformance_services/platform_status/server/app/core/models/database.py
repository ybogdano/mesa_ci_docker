#!/usr/bin/env python3

# Copyright (C) Intel Corp.  2014.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Yuriy Bogdanov <yuriy.bogdanov@intel.com>

import uuid
from datetime import datetime
from core.extensions import db
from flask_login import UserMixin
from sqlalchemy.dialects.postgresql import UUID

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    created = db.Column(db.DateTime(), default=datetime.now(), nullable=False)

class Connection(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, unique=True, default=0)
    ip = db.Column(db.String(50))
    date = db.Column(db.String(50))
    request_url = db.Column(db.String(500))
    return_code = db.Column(db.String(50))
    browser_type = db.Column(db.String(50))

class DeviceIndex(db.Model):
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    host_name = db.Column(db.String(50), unique=True, nullable=False)
    last_seen = db.Column(db.DateTime(),
                          default=datetime.now(),
                          onupdate=datetime.now(),
                          nullable=False)
    created = db.Column(db.DateTime(), default=datetime.now(), nullable=False)

class Devices(db.Model):
    id = db.Column(UUID(as_uuid=True), primary_key=True, unique=True)
    host_name = db.Column(db.String(50), unique=True, nullable=False)
    platform_system = db.Column(db.String(50))
    platform_release = db.Column(db.String(180))
    platform_version = db.Column(db.String(180))
    platform_machine = db.Column(db.String(28))
    platform_location = db.Column(db.String(180))
    platform_cpu_sn = db.Column(db.String(180))
    platform_label = db.Column(db.String(180))

    bios_vendor = db.Column(db.String(60))
    bios_release_date = db.Column(db.String(28))
    bios_version = db.Column(db.String(89))

    cpu_microcode = db.Column(db.String(12))
    cpu_arch = db.Column(db.String(12))
    cpu_vendor_id = db.Column(db.String(28))
    cpu_brand = db.Column(db.String(86))
    cpu_hz_advertised = db.Column(db.String(24))
    cpu_hz_actual = db.Column(db.String(24))
    cpu_stepping = db.Column(db.Integer)
    cpu_model = db.Column(db.Integer)
    cpu_family = db.Column(db.Integer)
    cpu_physical_core_count = db.Column(db.Integer)
    cpu_logical_core_count = db.Column(db.Integer)

    installed_memory_total = db.Column(db.BigInteger)
    installed_memory_available = db.Column(db.BigInteger)
    installed_memory_used = db.Column(db.BigInteger)
    installed_memory_free = db.Column(db.BigInteger)
    installed_memory_percent = db.Column(db.Float(4))

    swap_memory_total = db.Column(db.BigInteger)
    swap_memory_used = db.Column(db.BigInteger)
    swap_memory_free = db.Column(db.BigInteger)
    swap_memory_percent = db.Column(db.Float(4))

    gpu = db.Column(db.String(1600))

    disk_usage_total = db.Column(db.BigInteger)
    disk_usage_used = db.Column(db.BigInteger)
    disk_usage_free = db.Column(db.BigInteger)
    disk_usage_percent = db.Column(db.Float(4))

    uptime = db.Column(db.String(26))


class DiskPartition(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    disk_id = db.Column(UUID(as_uuid=True))
    disk_number = db.Column(db.String(60), default=0)
    disk_dev = db.Column(db.String(240))
    disk_mount = db.Column(db.String(240))
    disk_filesystem = db.Column(db.String(24))


class Networks(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    network_id = db.Column(UUID(as_uuid=True))
    network_number = db.Column(db.String(60), default=0)
    network_name = db.Column(db.String(24))
    network_ip = db.Column(db.String(24))
    network_mac = db.Column(db.String(24))


class PeripheralDevices(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    peripheral_id = db.Column(UUID(as_uuid=True))
    peripheral_number = db.Column(db.String(60), default=0)
    peripheral_device_id = db.Column(db.String(240))
    peripheral_tag = db.Column(db.String(240))
    peripheral_device = db.Column(db.String(240))


class DeviceUsers(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(UUID(as_uuid=True))
    user_number = db.Column(db.Integer, default=0)
    user_name = db.Column(db.String(24))
    user_terminal = db.Column(db.String(24))
    user_host = db.Column(db.String(24))
    user_time = db.Column(db.Float)
    user_pid = db.Column(db.Integer)

class GlobalInfo(db.Model):
    id = db.Column(db.Integer, default=1, primary_key=True)
    file_version_sha = db.Column(db.String(64))
    platform_poll_time_sec = db.Column(db.Integer, default=60)
    platform_disabled = db.Column(db.Boolean, default=False)
    last_updated = db.Column(db.DateTime())
