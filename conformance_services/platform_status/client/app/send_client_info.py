#!/usr/bin/python3

import os
import platform
import time
import datetime
import re
import hashlib
import socket
import shutil
import subprocess
import json
import psutil
import requests
import sys

platform_poll_time_sec = 10
update_cycle = 0 # 0 - is disabled

debug = False
localurl = 'http://127.0.0.1:10201'
serverurl = 'http://192.168.1.1:10201'
file_url = 'http://mesa-ci-files.jf.intel.com/client-info/app/send_client_info.py'
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

if debug:
    serverurl = localurl


def send_to_dmesg(message):
    process = subprocess.Popen(["dmesg", "-n"], stdin=subprocess.PIPE)
    process.communicate(input=message.encode())


def get_status():
    global platform_poll_time_sec
    try:
        global_conf = json.loads(requests.get(os.path.join(serverurl, 'version'),
                                              timeout=15).content.decode())
    except requests.exceptions.Timeout as err:
        print(f"ERROR: Failed to establish connection with error: {err}")

    if int(global_conf['time']) != platform_poll_time_sec and global_conf is not None:
        platform_poll_time_sec = int(global_conf['time'])

    local_file_sha = hashlib.sha256(open(os.path.abspath(__file__),
                                         'rb').read()).hexdigest()
    if not debug:
        if local_file_sha != global_conf['version']:
            print("New File detected, updating local file and restarting process")
            response = requests.get(file_url)
            with open(os.path.basename(__file__), 'wb') as f:
                f.write(response.content)
            service = subprocess.run(["systemctl", "restart", "client-info.service"], check=False)
            if service.returncode == 0:
                print("Send Client Info Service Restarted Secessfully!!!")
            else:
                print("Attempting ro restart the Program!!")
                os.execv(sys.argv[0], sys.argv)


def list_sessions():
    output = subprocess.check_output(['w', '-h']).decode('utf-8')
    sessions = output.split('\n')
    for session in sessions:
        if session:
            return session


def get_system_info():
    info = dict()
    # Platform
    uname_result = platform.uname()
    _platform = dict()
    _platform['system'] = uname_result.system
    _platform['node'] = uname_result.node
    _platform['release'] = uname_result.release
    _platform['version'] = uname_result.version
    _platform['machine'] = uname_result.machine
    info['platform'] = _platform

    # Running processes/services
    # _proc = list()
    # for p in psutil.process_iter():
    #     _proc.append(p.as_dict(attrs=['pid',
    #                                   'name',
    #                                   'username',
    #                                   'cpu_percent',
    #                                   'cpu_times',
    #                                   'memory_info'
    #                                   ]))
    # info['processes'] = _proc[0]

    # BIOS
    _bios = dict()
    _bios['vendor'] = subprocess.check_output("dmidecode --string bios-vendor",
                                              universal_newlines=True,
                                              shell=True).strip('\n')
    _bios['release_date'] = subprocess.check_output("dmidecode --string bios-release-date",
                                                    universal_newlines=True,
                                                    shell=True).strip('\n')
    _bios['version'] = subprocess.check_output("dmidecode --string bios-version",
                                               universal_newlines=True,
                                               shell=True).strip('\n')
    info['bios'] = _bios

    # CPU
    _cpu = dict()
    out = ((subprocess.check_output("lscpu", shell=True).strip()).decode()).split('\n')
    cpu_microcode = ((subprocess.check_output("journalctl -b -k | grep 'microcode:'", 
                                              shell=True).strip()).decode()).split('\n')
    for item in out:
        new_item = (" ".join(item.split())).split(':')
        _cpu[new_item[0]] = new_item[-1]
    for i in cpu_microcode:
        if "Current revision:" in i:
            cpu_microcode = i.split("Current revision: ")[-1].replace('0x', '')
        elif "microcode updated early to revision" in i:
            cpu_microcode = i.split("microcode updated early to revision ")[-1].replace('0x', '').split(',')[0]
        # else:
        #     print(f"UPDATE line 117: {i}\n")
    _cpu['cpu_microcode'] = cpu_microcode
    info['cpu'] = _cpu

    # Memory
    mem = {}
    swap = {}
    freem = (subprocess.check_output("free -b", shell=True).strip()).decode().split('\n')
    freem.pop(0)
    for item in freem:
        out = (" ".join(item.split())).split(':')
        _mem_out = out[-1].strip().split(' ')
        if 'Mem' in out:
            _mem_out_percent = round((int(_mem_out[1]) / int(_mem_out[0])) * 100, 2)
            mem['total'] = _mem_out[0]
            mem['used'] = _mem_out[1]
            mem['free'] = _mem_out[2]
            mem['shared'] = _mem_out[3]
            mem['cached'] = _mem_out[4]
            mem['available'] = _mem_out[5]
            mem['percent'] = _mem_out_percent
    if 'Swap' in out:
        _mem_out_percent = round((int(_mem_out[1]) / int(_mem_out[0])) * 100, 2)
        swap['total'] = _mem_out[0]
        swap['used'] = _mem_out[1]
        swap['free'] = _mem_out[2]
        swap['percent'] = _mem_out_percent
    info['installed_memory'] = mem
    info['swap_memory'] = swap

    # Disk, total, used, free, percent
    disk_usage = {}
    info['disk_partitions'] = psutil.disk_partitions()
    total, used, free = shutil.disk_usage("/")
    percent = round((used / total) * 100, 2)
    disk_usage['total'] = total
    disk_usage['used'] = used
    disk_usage['free'] = free
    disk_usage['percent'] = percent
    info['disk_usage'] = disk_usage
    # info['disk_usage'] = dict(psutil.disk_usage('/')._asdict())

    # GPU
    gpu = subprocess.check_output(
        r"lspci | grep ' VGA ' | cut -d' ' -f1 | xargs -i lspci -v -s {}",
        universal_newlines=True,
        shell=True
        )
    info['gpu'] = gpu.strip().split('\n')

    # Network
    _network = dict()
    _network_if_addrs = psutil.net_if_addrs()
    for _, key in enumerate(_network_if_addrs):
        _interfaces = dict()
        for snic in _network_if_addrs[key]:
            if snic.family == socket.AF_INET:
                _interfaces['ip_address'] = snic.address
            elif snic.family == psutil.AF_LINK:
                _interfaces['mac_address'] = snic.address
            _network[key] = _interfaces
    info['network'] = _network

    # Peripherals (USB connected devices)
    _devices = []
    device_re = re.compile(
        r"Bus\s+(?P<bus>\d+)\s+Device\s+(?P<device>\d+).+ID\s(?P<id>\w+:\w+)\s(?P<tag>.+)$",
        re.I
        )
    df = subprocess.check_output("lsusb", universal_newlines=True)
    for i in df.split('\n'):
        if i:
            _inf = device_re.match(i)
            if _inf:
                dinfo = _inf.groupdict()
                dinfo['device'] = f"/dev/bus/usb/{dinfo.pop('bus')}/{dinfo.pop('device')}"
                _devices.append(dinfo)
    info['peripheral_devices'] = _devices

    # Uptime
    time_since = datetime.datetime.strptime((subprocess.check_output("uptime -s",
                                                                 shell=True).strip()).decode(),
                                        '%Y-%m-%d %H:%M:%S')
    uptime = str(datetime.timedelta(seconds = int(time.time() - time_since.timestamp())))
    info['uptime'] = uptime

    # Users
    # 
    session = list(filter(None, list_sessions().split(' ')))
    # print(session, type(session))
    info['users'] = psutil.users()
    # print(info["users"])

    return info

try:
    while True:
        if update_cycle != 0: # Dev loop
            for cycle in range(update_cycle):
                get_status()
                try:
                    r = requests.post(os.path.join(serverurl, 'platform'),
                                    data=json.dumps(get_system_info()),
                                    headers=headers,
                                    timeout=15
                                    )
                except requests.exceptions.ConnectionError as err:
                    print(f"Could not connect to server.\
                        Trying again in {int( platform_poll_time_sec / 60) } min.")
                print(f"sleeping for: {platform_poll_time_sec} sec, Loop #: {cycle}")
                time.sleep(platform_poll_time_sec)
            break
        else: # Prod loop
            get_status()
            try:
                r = requests.post(os.path.join(serverurl, 'platform'),
                                    data=json.dumps(get_system_info()),
                                    headers=headers,
                                    timeout=15
                                    )
            except requests.exceptions.ConnectionError as err:
                print(f"Could not connect to server.\
                    Trying again in {int( platform_poll_time_sec / 60) } min.")

            # print(f"sleeping for: {platform_poll_time_sec} sec")
            time.sleep(platform_poll_time_sec)

except KeyboardInterrupt:
    print("Stopping Service, Send to server")
